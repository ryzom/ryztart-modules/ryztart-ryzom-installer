import os
import re
import sys
import json
import stat
import lzma
import time
import psutil
import shutil
import locale
import appdirs
import hashlib
import platform
import humanize
import subprocess
import datetime as dt
import xml.etree.ElementTree as ET

from os import path as p
from queue import Queue
from threading import Thread
from pyquery import PyQuery as pq
from dateutil import parser

from kyssModule import KyssModule, KyssConfigIni

INI_LAST_VERSION = 2

XML_VERSION = 0
XML_FILESIZE = 1
XML_7ZFILESIZE = 2
XML_FILETIME = 3
XML_PATCHSIZE = 4

def toRyzomPatchHex(xml_version):
	final = ""
	for i in range(5):
		h = hex(xml_version[5+i]).split("x")[-1].zfill(8)
		final += h[6:8]+h[4:6]+h[2:4]+h[0:2]
	return final


def getClientName():
	if platform.system() == "Darwin":
		return "Ryzom.app/Contents/MacOS/Ryzom"
	
	if platform.system() == "Linux":
		return "ryzom_client"
	
	return "ryzom_client_r.exe"

def getConfiguratorName():
	if platform.system() == "Darwin":
		return "Ryzom.app/Contents/MacOS/Ryzom"
	
	if platform.system() == "Linux":
		return "ryzom_configuration_qt"
	
	return "ryzom_configuration_qt_r.exe"


def getPatcherName():
	if platform.system() == "Darwin":
		return "RyzomClientPatcher"
	
	if platform.system() == "Linux":
		return "ryzom_client_patcher"
	
	return "ryzom_client_patcher.exe"

def getShardName(name, default="Atys"):
		domains = {"ryzom_live" : "Atys", "ryzom_dev" : "Yubo", "ryzom_test" : "Gingo"}
		if name in domains:
			return domains[name]
		return default


class RyzomClientInstaller(KyssModule):

	def _init(self):
		self.html_id = "ryzom_installer"

		self.download_queue = Queue(0)
		self.uncompress_queueA = Queue(0)
		self.uncompress_queueB = Queue(0)
		self.writedisk_queue = Queue(0)
		
		self.download_worker = Thread(target=self._downloadFilesFromQueue)
		self.uncompress_workerA = Thread(target=self._uncompressFilesFromQueueA)
		self.uncompress_workerB = Thread(target=self._uncompressFilesFromQueueB)
		self.writedisk_worker = Thread(target=self._writediskFilesFromQueue)
		
		self.download_worker.setDaemon(True)
		self.uncompress_workerA.setDaemon(True)
		self.uncompress_workerB.setDaemon(True)
		self.writedisk_worker.setDaemon(True)

		self.download_worker.start()
		self.uncompress_workerA.start()
		self.uncompress_workerB.start()
		self.writedisk_worker.start()

		ryzom_platforms = {"Linux": "linux", "Darwin" : "osx", "Windows": "win"}

		if platform.system() == "Darwin":
			exedll_name = "exedll_osx.bnp"
		elif sys.maxsize > 2**32:
			exedll_name = "exedll_"+ryzom_platforms[platform.system()]+"64.bnp"
		else:
			exedll_name = "exedll_"+ryzom_platforms[platform.system()]+"32.bnp"
		
		self.unpackables = ("fonts.bnp", "packedsheets.bnp", "cfg.bnp", exedll_name)
	
	
	def _downloadFilesFromQueue(self):
		while True:
			eid, patch_live_url, filename, patches, last_version, destination, unpack_to = self.download_queue.get()
			self.log("Downloading {}...".format(filename))
			self.setE("#-status", self._("dowloading")+" <span style='color: #ffffff'>{}</span>".format(filename))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"white\"")
			self._downloadAndInstallFile(eid, patch_live_url, filename, patches, last_version, destination, unpack_to)
			self.download_queue.task_done()
	
	def _uncompressFiles(self, eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime):
		self.log("Uncompressing {}...".format(filename))
		self.setE("#-status", self._("uncompressing")+" <span style='color: #ffffff'>{}</span>...".format(filename))
		self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"yellow\"")
		uncompressed = lzma.decompress(compressed)
		local_sha1 = hashlib.sha1(uncompressed).hexdigest()
		if local_sha1 == wanted_sha1:
			self.log("Same sha1 {}".format(filename))
			self.writedisk_queue.put((eid, destination, filename, uncompressed, unpack_to, mtime))
		else:
			self.error("Bad sha1 {}".format(filename))
	
		
	def _uncompressFilesFromQueueA(self):
		while True:
			eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime = self.uncompress_queueA.get()
			self._uncompressFiles(eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime)
			self.uncompress_queueA.task_done()

	def _uncompressFilesFromQueueB(self):
		while True:
			eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime = self.uncompress_queueB.get()
			self._uncompressFiles(eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime)
			self.uncompress_queueB.task_done()

	def _writediskFilesFromQueue(self):
		self.need_finish = 2
		while True:
			eid, destination, filename, uncompressed, unpack_to, mtime = self.writedisk_queue.get()
			self.log("Writing {}".format(filename))
			self.setE("#-status", self._("writing_to_disk")+" <span style='color: #ffffff'>{}</span>...".format(filename))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"lightgreen\"")
			if not unpack_to:
				with open(destination+os.sep+"data"+os.sep+filename, "wb") as f:
					f.write(uncompressed)
				os.utime(destination+os.sep+"data"+os.sep+filename, (mtime, mtime))
			else:
				self.log("Unpack {}".format(filename))
				self.setE("#-status", self._("unpacking")+" <span style='color: #ffffff'>{}</span>...".format(filename))
				with open(destination+os.sep+"unpack"+os.sep+filename, "wb") as f:
					f.write(uncompressed)
				self._unpackFile(destination, filename, unpack_to)
				os.utime(destination+os.sep+"unpack"+os.sep+filename, (mtime, mtime))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"green\"")
			self.writedisk_queue.task_done()

	def _setReadyToPlay(self, domain):
		self.setE("#-button", "<img style='margin:0;width:32px' src='file:data/icons/play_black.svg' /><span style='line-height: 15px'>"+self._("play")+"</span>")
		self.setE("#-tooltip", self._("start_play_ryzom"))
		self.setupE("#-button", "onclick", "function() { pywebview.api.run('"+self.html_id+"', 'Play'); return false }")
		self.setE("#-infos", "{} - ".format(getShardName(domain))+self._("ryzom_uptodate")+" {}".format(self.patch_version))
		self.state = "waiting"

	def _getPatchLiveUrl(self, domain):
		if domain == "ryzom_dev":
			return "https://yubo.ryzom.com/patch_live/"
		return "https://gingo.ryzom.com/patch_live/"

	def _getPatchVersion(self, domain, destination):
		patch_live_url = self._getPatchLiveUrl(domain)
		url = patch_live_url + "%s.version" % domain
		if not p.isdir(destination+os.sep+"unpack"):
			os.makedirs(destination+os.sep+"unpack")

		dist_version = self.downloadFile(url)
		if dist_version:
			try:
				return int(dist_version.decode("utf-8").split(" ")[0])
			except:
				self.error("bad remote version")
		return 0
	
	def _getLocalPatchVersion(self, domain, destination):
		if p.isfile(destination+"/unpack/%s.version" % domain):
			with open(destination+"/unpack/%s.version" % domain, "rb") as f:
				try:
					return int(f.read().decode("utf-8").split(" ")[0])
				except:
					self.error("bad local version")
		return -1

	def _getPatchList(self, domain, destination):
		# Get the patch version
		patch_live_url = self._getPatchLiveUrl(domain)
		
		self.log("Patch Version : {}".format(self.patch_version))
		self.log("Downloading version xml...")
		xml = self.downloadFile(patch_live_url+"%05d/ryzom_%05d_debug.xml" % (self.patch_version, self.patch_version))
		if not xml:
			return ({}, [])
		self.log("Downloading idx...")
		patch_version = self.downloadFile(patch_live_url+"%05d/ryzom_%05d.idx" % (self.patch_version, self.patch_version))
		if patch_version:
			with open(destination+"/unpack/ryzom_%05d.idx" % self.patch_version, "wb") as f:
				f.write(patch_version)

		self.log("Getting bnp versions from xml...")
		root = ET.fromstring(xml)
		total_files = 0
		files = {}
		for x in root.findall("_Files/_Files"):
			versions = []
			for v in x.findall("_Versions"):
				values = []
				for val in v:
					values.append(int(val.get("value")))
				versions.append(values)
			files[x[0].get("value")] = versions
			total_files += 1

		self.log("Getting unpack informations from xml...")
		unpack_files = {} #Get bnp to unpack
		for x in root.findall("_Categories/_Category"):
			bnp_name = ""
			for v in x.findall("_Files"):
				bnp_name = v.get("value")

			for v in x.findall("_UnpackTo"):
				unpack_files[bnp_name] = v.get("value") #Unpack .ref files, not the bnps
		self.log("All done!")
		return (files, unpack_files)

	def _checkDownloadedFileContent(self, filename, downloaded):
		self.total_downloaded += downloaded
		percent = (100*self.total_downloaded) / self.total_to_download
		if filename and percent % 10 == 0:
			self.setE("#-status", self._("downloading")+" <span style='color: #ffffff'>{}</span>".format(filename))
		self.setupE("#-file_progress", "style.width", "\"{}%\"".format(round(percent)))

	def _checkDownloadedAllFiles(self, downloaded):
		self.total_downloaded += downloaded
		percent = (100*self.total_downloaded) / self.total_to_download
		self.setupE("#-progress", "style.width", "\"{}%\"".format(round(percent)))

	def _downloadAndInstallFile(self, eid, patch_live_url, filename, patches, last_version, destination, unpack_to=""):
		download = patches == []
		if patches:
			self.total_downloaded = 0
			self.total_to_download = len(patches)
			self.setE("#-status", self._("patching")+" <span style='color: #ffffff'>{}</span>".format(filename))
			for patch in patches:
				url = patch_live_url+"%05d/%s_%05d.patch" % (int(patch[XML_VERSION]), p.splitext(filename)[0], int(patch[XML_VERSION]))
				content = self.downloadFile(url)
				if content == None:
					#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
					download = True
					break
					
				with open(destination+"/unpack/"+filename+".patch", "wb") as f:
					f.write(content)
				
				if p.isfile(destination+"/unpack/"+filename):
					os.remove(destination+"/unpack/"+filename)
				
				if platform.system() == "Windows":
					dest = destination.encode("utf-8").decode("cp1252")
				else:
					dest = destination
				
				result = subprocess.run(["modules/ryzom_installer/"+getPatcherName(), "-p", dest+"/unpack/"+filename+".patch", "-s", dest+"/data/"+filename, "-d", dest+"/unpack/"+filename], capture_output=True)
				if result.returncode == 0:
					try:
						os.remove(destination+"/unpack/"+filename+".patch")
						os.remove(destination+"/data/"+filename)
						os.rename(destination+"/unpack/"+filename, destination+"/data/"+filename)
						os.utime(destination+"/data/"+filename, (last_version[XML_FILETIME], last_version[XML_FILETIME]))
					except:
						self.error("Error in patch")
						download = True
						break
					self.log("File patched successfully to version {}{}{}".format(self.color("white"), patch[XML_VERSION], self.color("reset")))
					self._checkDownloadedFileContent("", 1)
				else:
					self.error("Error in patch")
					download = True
					break
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"green\"")

		if download:
			url = patch_live_url+"%05d/%s.lzma" % (int(last_version[XML_VERSION]), filename)
			self.total_downloaded = 0
			self.total_to_download = int(last_version[XML_7ZFILESIZE])
			content = self.downloadFile(url, "GET", lambda x: self._checkDownloadedFileContent(filename, x))
			if content == None:
				#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
				return
			
			if self.uncompress_queueA.empty():
				self.uncompress_queueA.put((eid, destination, filename, content, toRyzomPatchHex(last_version), unpack_to, last_version[XML_FILETIME]))
			else:
				self.uncompress_queueB.put((eid, destination, filename, content, toRyzomPatchHex(last_version), unpack_to, last_version[XML_FILETIME]))

	def _unpackFile(self, destination, filename, unpack_to):
		try:
			if unpack_to:
				if not p.isdir(destination+os.sep+"unpack"+os.sep+"tmp/"):
					os.makedirs(destination+os.sep+"unpack"+os.sep+"tmp/")
				if platform.system() == "Windows":
					dest = destination.encode("utf-8").decode("cp1252")
				else:
					dest = destination
				result = subprocess.call(["modules"+os.sep+"ryzom_installer"+os.sep+getPatcherName(), "-u", dest+os.sep+"unpack"+os.sep+filename, "-d", dest+os.sep+"unpack"+os.sep+"tmp"])
			else:
				if platform.system() == "Windows":
					dest = destination.encode("utf-8").decode("cp1252")
				else:
					dest = destination
				result = subprocess.call(["modules"+os.sep+"ryzom_installer"+os.sep+getPatcherName(), "-u", dest+os.sep+"unpack"+os.sep+filename, "-d", dest+unpack_to[1:]])
		except:
			self.error("Can't unpack {}".format(filename))
			result = 1

		if result == 0:
			filename_no_ext, filename_ext = p.splitext(filename)
			if filename in self.unpackables:
				for unpacked_file in os.listdir(destination+os.sep+"unpack"+os.sep+"tmp"):
					if not p.isfile(destination+os.sep+"unpack"+os.sep+"tmp"+os.sep+unpacked_file):
						continue
					unpacked_filename, unpacked_file_ext = p.splitext(unpacked_file)
					if not p.isdir(destination+unpack_to[1:]):
						os.makedirs(destination+unpack_to[1:])
					self.log("Move {} to {}".format(destination+os.sep+"unpack"+os.sep+"tmp"+os.sep+unpacked_file, destination+unpack_to[1:]+unpacked_file))
					try:
						os.replace(destination+os.sep+"unpack"+os.sep+"tmp"+os.sep+unpacked_file, destination+unpack_to[1:]+unpacked_file)
					except:
						self.error(destination+os.sep+"unpack"+os.sep+"tmp"+os.sep+unpacked_file)
						pass
					if platform.system() == "Linux": # maybe on macos too
						if unpacked_file == getClientName():
							# made client exectuable
							st = os.stat(destination+unpack_to[1:]+unpacked_file)
							os.chmod(destination+unpack_to[1:]+unpacked_file, st.st_mode | stat.S_IEXEC)
		else:
			#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
			pass

	def checkNeedUpdate(self, destination, domain, force=False):
		if not (self.download_queue.empty() and self.uncompress_queueA.empty() and self.uncompress_queueB.empty() and self.writedisk_queue.empty()):
			return

		self.log("Cheking update in folder {} for {}".format(destination, domain))
		if not p.isdir(destination):
			os.makedirs(destination)
		
		# Check if need patch or not
		self.patch_version = self._getPatchVersion(domain, destination)
		self.local_version = self._getLocalPatchVersion(domain, destination)
		
		self.log("Remote patch version: {}, Local patch version: {}".format(self.patch_version, self.local_version))
		if self.patch_version == self.local_version and not force:
			self._setReadyToPlay(domain)
			return
				
		files, unpack_files = self._getPatchList(domain, destination)

		patch_live_url = self._getPatchLiveUrl(domain)

		self.workers = []
		i = len(files.items())+1
		patches_to_do = []
		
		if platform.system() == "Linux" and not p.isfile(destination+os.sep+"libopenal.so.1"):
			shutil.copyfile("modules/ryzom_installer/libopenal.so.1."+platform.machine(), destination+os.sep+"libopenal.so.1")

		if platform.system() == "Windows" and not p.isfile(destination+os.sep+"msvcp120.dll"):
			shutil.copyfile("modules/ryzom_installer/msvcp120.dll", destination+os.sep+"msvcp120.dll")
		if platform.system() == "Windows" and not p.isfile(destination+os.sep+"msvcr120.dll"):
			shutil.copyfile("modules/ryzom_installer/msvcr120.dll", destination+os.sep+"msvcr120.dll")
		
		total_size = 0
		all_files = files.items()
		
		self.setE("#-infos", self.getTemplateV2("ryzom_installer:progression"))

		total_files = len(all_files)
		i = 0
		
		# Clean unpack
		self.log("Cleaning unpack...")
		for filename in os.listdir(destination+os.sep+"unpack"):
			if p.isfile(destination+os.sep+"unpack"+os.sep+filename) and not filename in self.unpackables and filename != "%s.version" % domain and filename != "ryzom_%05d.idx" % self.patch_version:
				try:
					os.remove(destination+os.sep+"unpack"+os.sep+filename)
				except:
					self.warning("Can't delete {} in unpack/".format(filename))
		self.log("done!")

		self.log("Start checking of files...")
		for filename, versions in all_files:
			i += 1
			version = versions[-1]
			total_size += version[2]
			filename_no_ext, filename_ext = p.splitext(filename)
			if (filename[:6] != "exedll" or filename in self.unpackables) and filename_ext != ".ref":
				self.setupE("#-progress-text", "innerText", "\""+self._("checking")+" {}...\"".format(filename))
				self.setupE("#-progress-bar", "style.width", "\"{}%\"".format(round((100 * i) / total_files)))
				if filename in self.unpackables:
					# Download lastest sha1
					sha1 = self.downloadFile(patch_live_url+"%05d/%s.sha1" % (self.patch_version, filename_no_ext))
					if p.isfile(destination+os.sep+"unpack"+os.sep+filename):
						os.utime(destination+os.sep+"unpack"+os.sep+filename, (version[XML_FILETIME], version[XML_FILETIME]))
						uncorrect_file = False
					else:
						uncorrect_file = True
					unpack_to = unpack_files[filename]
					for h in sha1.decode("utf-8").split("\n"):
						if not h:
							continue
						sh = h.split(" ")
						if len(sh) == 3:
							remote_sha1 = sh[0]
							unpack_file = sh[2]
							if p.isfile(destination+unpack_to[1:]+os.sep+unpack_file):
								with open(destination+unpack_to[1:]+os.sep+unpack_file, "rb") as f:
									local_sha1 = hashlib.sha1(f.read()).hexdigest()
									if local_sha1 != remote_sha1:
										uncorrect_file = True
							else:
								self.log("File not found: {}".format(destination+unpack_to[1:]+os.sep+unpack_file))
								uncorrect_file = True
								
					versions = files[filename]
					if uncorrect_file:
						self.warning("{} need patch...".format(filename))
						patches_to_do.append([filename, [], versions[-1], unpack_to])
				else:
					destination_file = destination+os.sep+"data"+os.sep+"%s" % filename

					vers = versions[-1]
					remote_sha1 = toRyzomPatchHex(vers)
					unpack_to = ""
					if filename in unpack_files:
						unpack_to = unpack_files[filename]
					
					if not p.isfile(destination_file):
						self.log("File not found")
						patches_to_do.append([filename, [], versions[-1], unpack_to])
					else:
						stats = os.stat(destination_file)
						if version[1] != stats.st_size or version[XML_FILETIME] != stats.st_mtime: # something wrong with this file (size, mtime or both)
							with open(destination+os.sep+"data"+os.sep+"%s" % filename, "rb") as f:
								local_sha1 = hashlib.sha1(f.read()).hexdigest()
							have_version = None
							
							if remote_sha1 == local_sha1: # in this case, mtime are different but it's the same file. So, we update the mtime
								os.utime(destination+os.sep+"data"+os.sep+"%s" % filename, (version[XML_FILETIME], version[XML_FILETIME]))
								continue
							
							for v in range(len(versions) - 1): # get the version of the file (if possible)
								vers = versions[v]
								remote_sha1 = toRyzomPatchHex(vers)
								if remote_sha1 == local_sha1:
									self.log("Found the version of this file: {}".format(vers[XML_VERSION]))
									have_version = v+1
									break
							
							patches_needed = []

							if have_version:
								size_of_all_patches = 0
								for v in range(len(versions)-have_version):
									patches_needed.append(versions[have_version+v])
									size_of_all_patches += int(versions[have_version+v][XML_PATCHSIZE])
								
								if int(versions[-1][XML_7ZFILESIZE]) < size_of_all_patches: # The sum of all patches are higher than the 7z file. So don't use patchs!
									self.log("Too much patchs, better download full file")
								else:
									self.log("This file need {} patches".format(len(patches_needed)))
							else:
								self.warning("File not found in versions")

							patches_to_do.append([filename, patches_needed, versions[-1], unpack_to])
		
		current_version = 99999
		total_patches = 0
		for ptodo in patches_to_do:
			total_patches += len(ptodo[1])
			if ptodo[2] and ptodo[2][0] < current_version:
				current_version = ptodo[2][0]

		self.patch_destination = destination
		if patches_to_do:
			with open(self.patch_destination+os.sep+"show_eula", "w") as f:
				f.write("1")
			
			self.patches_to_do = patches_to_do
			self.setE("#-button", self._("update"))
			self.setE("#-tooltip", self._("need_update"))
			patch_change = "to v{}".format(self.patch_version)
			if current_version < self.patch_version:
				patch_change = "from v{} ".format(current_version)+patch_change
			self.setE("#-infos", ("<span class='message'>"+self._("patching")+" {}</span><br />{} "+self._("files_need_update")+". {} "+self._("patches_to_do")).format(patch_change, len(patches_to_do), total_patches))
			self.setupE("#-button", "onclick", "function() {{ pywebview.api.run('{}', 'Update', '{}'); return false }}".format(self.html_id, domain))
		else:
			if self.patch_version != self.local_version:
				with open(destination+"/unpack/%s.version" % domain, "wb") as f:
					f.write("{} {}".format(self.patch_version, self.patch_version).encode("utf-8"))
			self._setReadyToPlay(domain)
	
	def udpateFiles(self, domain):
		if not p.isdir(self.patch_destination+os.sep+"data"):
			os.makedirs(self.patch_destination+os.sep+"data")

		if not p.isdir(self.patch_destination+os.sep+"unpack"):
			os.makedirs(self.patch_destination+os.sep+"unpack")
		
		self.setE("#-infos", "<div id='"+self.html_id+"-status'></div><div id='"+self.html_id+"-file_progress'></div><div id='"+self.html_id+"-slots'></div>")
		eid = 0
		for filename, patches_needed, version, unpack_to in self.patches_to_do:
			if filename:
				self.addToE("#-slots", "<div id='{}-status_{}' class='{}-bnpslot'><span class='tooltip'>{}</span></div>".format(self.html_id, eid, self.html_id,  filename[:-4]))
				self.download_queue.put((eid, self._getPatchLiveUrl(domain), filename, patches_needed, version, self.patch_destination, unpack_to))
				eid += 1
		self.addToE("#-slots", "<div style='clear: both'></div>")
		self.download_queue.join()
		self.uncompress_queueA.join()
		self.uncompress_queueB.join()
		self.writedisk_queue.join()
		self.log("Update finished !")
		with open(self.patch_destination+"/unpack/%s.version" % domain, "w") as f:
			f.write("{} {}".format(self.patch_version, self.patch_version))
		self._setReadyToPlay(domain)


class RyzomInstaller(KyssModule):

	def _init(self):
		self.state = "waiting"
		self.first_login = True
		self.logged = False
		self.servers = None
		self.profiles = None
		self.need_setup_main = False
		self.ryzom_roaming_path = appdirs.user_data_dir("Ryzom", "", roaming=True)
		self.ryzom_user_path = appdirs.user_data_dir("Ryzom", "", roaming=False)
		self.config = KyssConfigIni(self.ryzom_roaming_path+"/ryzom.ini")
		self.version = int(self.config.get("latest", "version", "1"))
		if self.version < 2:
			self.config.reset()
			self.config.set("latest", "version", str(INI_LAST_VERSION))
		self.clientInstaller = RyzomClientInstaller()
		self.clientInstaller.config = self.config
	
	def getUsedZones(self):
		return {"header": 1, "main": 500, "menu": 100}

	def getRunningClients(self):
		clients = []
		processName = getClientName()
		for proc in psutil.process_iter():
			try:
				if processName.lower() in proc.name().lower():
					clients.append((proc.pid, proc.name(), proc.cmdline(),))
			except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
				pass
		return clients

	def start(self):
		self.config.refresh()

		self.selected_profile = ""
		self.setE("#-select-profiles", "")
		self.hideE("#-select-profiles")

		cfg_ini = self.getModule("ryzom_config_ini")

		all_profiles = cfg_ini.getProfilesFromIni()
		self.log("profiles={}".format(all_profiles))
		
		profiles_atys = profiles_yubo = profiles_gingo = []
		if "Atys" in all_profiles:
			profiles_atys = self.templatize(all_profiles["Atys"], ("name", "shard", "folder"))
		if "Yubo" in all_profiles:
			profiles_yubo = self.templatize(all_profiles["Yubo"], ("name", "shard", "folder"))
		if "Gingo" in all_profiles:
			profiles_gingo = self.templatize(all_profiles["Gingo"], ("name", "shard", "folder"))
		
		if profiles_atys:
			selected_profile = profiles_atys[0]["_id_"]
		else:
			selected_profile = ""
			
		self.setE("#-select-profiles", self.getTemplateV2("profiles", {
			"profiles_atys": profiles_atys,
			"profiles_yubo": profiles_yubo,
			"profiles_gingo": profiles_gingo,
			"profiles_yubo?": len(profiles_yubo) > 0,
			"profiles_gingo?": len(profiles_gingo) > 0,
			}))
		self.showE("#-select-profiles")

		self.call_SelectProfile(selected_profile)



	def call_SelectShard(self, shard):
		cfg_ini = self.getModule("ryzom_config_ini")
		self.log("Selecting shard {}".format(shard))
		domain = cfg_ini.getDomainName(shard)
		servers = cfg_ini.getServersFromIni()

		if shard in servers and type(servers[shard]) == str:
			server_location = servers[shard]
		else:
			server_location = ""

		if not p.isdir(server_location):
			time.sleep(1)
			self.getModule("ryzom_content_manager").call_ShowConfig("servers")
			return

		if domain == "ryzom_live" and self.config.get("beta", "enabled") == "1":
			domain = "ryzom_beta"

		if len(self.getRunningClients()) > 0:
			self.setE("#-infos", self._("patch_warning_client_running"))

		self.state = "checking"
		if platform.system() == "Darwin":
			self.server_location = server_location+"/Ryzom.app/Contents/Resources"
		else:
			self.server_location = server_location

		self.selected_shard = shard
		self.domain = domain
		self.log(self.server_location)
		self.clientInstaller.checkNeedUpdate(self.server_location, self.domain)
		self.state = "waiting"

	def call_SelectProfile(self, profile):
		self.log("Select profile {}".format(profile))
		self.selected_shortcut = ""
		self.setE("#-select-shortcuts", "")
		self.hideE("#-select-shortcuts")
		selected_shard = "Atys"
		cfg_ini = self.getModule("ryzom_config_ini")
		self.selected_profile = profile
		all_profiles = cfg_ini.getProfilesFromIni(True)
		for shard_name, shard_profiles in all_profiles.items():
			if profile in shard_profiles:
				selected_shard = shard_name

		shortcuts = cfg_ini.getShortcutsFromIni(profile, selected_shard, skip_disabled=True)
		
		if profile and shortcuts:
			shortcuts = self.templatize(shortcuts, ("name", "args"))
			if self.config.get(profile, "last_shortcut"):
				self.selected_shortcut = self.config.get(profile, "last_shortcut")
				self.log("Select last shortcut {}".format(self.selected_shortcut))
			elif shortcuts:
				self.selected_shortcut = shortcuts[0]["_id_"]
				self.log("Select shortcut {}".format(self.selected_shortcut))
			
			for i in range(len(shortcuts)):
				if shortcuts[i]["_id_"] == self.selected_shortcut:
					shortcuts[i]["selected"] = "selected=\"selected\""
				else:
					shortcuts[i]["selected"] = ""

			if len(shortcuts) > 0:
				self.setE("#-select-shortcuts", self.getTemplateV2("shortcuts", {"shortcuts": shortcuts}))
				self.showE("#-select-shortcuts")
		
		self.call_SelectShard(selected_shard)

	def call_SelectShortcut(self, shortcut):
		self.selected_shortcut = shortcut

	
	def call_Setup(self):
		self.setup()

	def call_Update(self, domain):
		if self.state == "updating" or self.state == "checking":
			return
			
		self.setE("#-button", self._("please_wait"))
		self.state = "updating"
		self.clientInstaller.udpateFiles(domain)

	def call_CheckNeedUpdate(self):
		self.clientInstaller.checkNeedUpdate(self.server_location, self.domain, True)

	def call_OpenLoginPopup(self, token):
		self.switchE("#-menu-content")

	def call_LogOut(self, token):
		self.logoutWindow = KyssModule.webview.create_window("Ryzom "+self._("logout"), url="https://me.ryzom.com/api/oauth/logout?access_token="+token)
		self.logoutWindow.loaded += self.onLoadedLogoutWindow

	def call_LogIn(self):
		try:
			self.authWindow = KyssModule.webview.create_window("Ryzom "+self._("login"),  width=1024, height=650, icon="data/icon", url="https://me.ryzom.com/api/oauth/authorize?response_type=code&client_id=ryztart&redirect_uri=https%3A%2F%2Fme.ryzom.com%2Fapi%2Foauth%2Fcallback&scope=ryztart&state=ryztart&lang="+KyssModule.lang)
		except:
			self.authWindow = KyssModule.webview.create_window("Ryzom "+self._("login"),  width=1024, height=650,  url="https://me.ryzom.com/api/oauth/authorize?response_type=code&client_id=ryztart&redirect_uri=https%3A%2F%2Fme.ryzom.com%2Fapi%2Foauth%2Fcallback&scope=ryztart&state=ryztart&lang="+KyssModule.lang)

		self.authWindow.closed += self.onCloseAuthWindow
		self.authWindow.loaded += self.onLoadedAuthWindow

	
	def call_Play(self):
		cfg_ini = self.getModule("ryzom_config_ini")
		
		self.log("Selected [{}] [{}] [{}]".format(self.selected_shard, self.selected_profile, self.selected_shortcut))
		
		all_profiles = cfg_ini.getProfilesFromIni()
		profiles = {}
		for shard_name, shard_profiles in all_profiles.items():
			for profile_name, profile in shard_profiles.items():
				profiles[profile_name] = profile
		shortcuts = cfg_ini.getShortcutsFromIni(self.selected_profile)

		if self.selected_profile in profiles:
			self.config.set(self.selected_profile, "last_shortcut", self.selected_shortcut)
			self.config.save()
			folder = profiles[self.selected_profile][1]
			shard = profiles[self.selected_profile][2]
		else:
			folder = ""
			shard = "Atys"

		ryzom_path = self.config.get(shard, "location")
		if not ryzom_path:
			ryzom_path = self.config.get("Atys", "location")
		if not ryzom_path or not p.isdir(ryzom_path):
			time.sleep(1)
			self.getModule("ryzom_content_manager").call_ShowConfig("servers")
			return

		
		if self.selected_shortcut in shortcuts:
			args = shortcuts[self.selected_shortcut][1]
		else:
			args = ""
	
		cfg_ini.updateLangInProfile(shard, KyssModule.lang, folder)
	
		client_name = getClientName()
		client_args = [client_name]
		
		if folder != "":
			client_args.append("--profile")
			client_args.append(folder)

		user_args = args.split(" ")
		client_args += user_args
		
		running_clients = self.getRunningClients()
		for running_client in running_clients:
			if user_args[0] != "" and len(running_client[2]) > 3 and running_client[1] == folder and user_args[0] == running_client[2][3]:
				self.setE("#-infos", "<span class='error'>"+self._("account_already_connected")+"</span><br />".format(len(running_clients)+1))
				return
		
		shell = True
		if platform.system() == "Linux":
			# made client exectuable
			client_args[0] = ryzom_path+os.sep+client_args[0]
			st = os.stat(ryzom_path+"/"+client_name)
			os.chmod(ryzom_path+"/"+client_name, st.st_mode | stat.S_IEXEC)
			shell = False
		elif platform.system() == "Darwin":
			shell = False

		client_args.append("--nopatch")
		client_args.append("1")

		# Check if require EULA and move it to profile folder
		if p.isfile(ryzom_path+os.sep+"show_eula"):
			shutil.move(ryzom_path+os.sep+"show_eula", self.ryzom_roaming_path+os.sep+folder+os.sep+"show_eula")

		self.log("Run in {}: {}".format(ryzom_path, " ".join(client_args)))
		self.state = "running"
		self.setE("#-infos", self._("running_clients", (len(running_clients)+1))+"<br />")

		subprocess.run(client_args, cwd=ryzom_path, shell=shell)

		running_clients = len(self.getRunningClients())
		if running_clients == 0:
			self.state = "waiting"
			self.setE("#-infos", self._("ready_to_play")+"<br />")
		else:
			self.setE("#-infos", self._("running_clients", running_clients+1)+"<br />")

	def call_Configure(self):
		cfg_ini = self.getModule("ryzom_config_ini")
		
		self.log("Selected [{}] [{}] [{}]".format(self.selected_shard, self.selected_profile, self.selected_shortcut))
		
		all_profiles = cfg_ini.getProfilesFromIni()
		profiles = {}
		for shard_name, shard_profiles in all_profiles.items():
			for profile_name, profile in shard_profiles.items():
				profiles[profile_name] = profile
		shortcuts = cfg_ini.getShortcutsFromIni(self.selected_profile)

		if self.selected_profile in profiles:
			self.config.set(self.selected_profile, "last_shortcut", self.selected_shortcut)
			self.config.save()
			folder = profiles[self.selected_profile][1]
			shard = profiles[self.selected_profile][2]
		else:
			folder = ""
			shard = "Atys"

		ryzom_path = self.config.get(shard, "location")
		if not ryzom_path:
			ryzom_path = self.config.get("Atys", "location")
		if not ryzom_path or not p.isdir(ryzom_path):
			time.sleep(1)
			self.getModule("ryzom_content_manager").call_ShowConfig("servers")
			return

		client_name = getConfiguratorName()
		client_args = [client_name]
		
		if folder != "":
			client_args.append("--profile")
			client_args.append(folder)
				
		shell = True
		if platform.system() == "Linux":
			# made client exectuable
			client_args[0] = ryzom_path+os.sep+client_args[0]
			st = os.stat(ryzom_path+"/"+client_name)
			os.chmod(ryzom_path+"/"+client_name, st.st_mode | stat.S_IEXEC)
			shell = False
		elif platform.system() == "Darwin":
			shell = False

		subprocess.run(client_args, cwd=ryzom_path, shell=shell)

	def onCloseAuthWindow(self):
		self.setup()
	
	def onLoadedLogoutWindow(self):
		self.logoutWindow.destroy()
		self.setup()

	def onLoadedAuthWindow(self):
		while self.authWindow:
			try:
				url = self.authWindow.get_current_url()
			except KeyError:
				return
				
			if not url:
				return

			if url[:37] == "https://me.ryzom.com/api/oauth/bypass":
				try:
					self.authWindow.destroy()
					self.setConfig("me", "uid", "skip")
					self.setConfig("me", "token", "skip")
				except:
					pass
				self.setup()
			elif url[:38] == "https://me.ryzom.com/api/oauth/success":
				token = self.authWindow.get_current_url().split("=")[1]
				userInfos = self.downloadFile("https://me.ryzom.com/api/oauth/user?access_token="+token)
				try:
					userInfos = json.loads(userInfos)
				except:
					pass
				self.setConfig("me", "uid", userInfos["id"])
				self.setConfig("me", "token", token)
				rci = self.getModule("ryzom_config_ini")
				rci.setup()
				try:
					self.authWindow.destroy()
				except:
					pass
			else:
				time.sleep(1)

	def setup(self):
		try:
			humanize.i18n.activate(locale.getdefaultlocale()[0])
		except:
			pass

		token = self.getConfig("me", "token")
		self.userInfos = {"name": "", "result": "ko", "message": "bad json"}
		if token and token != "skip":
			self.log("Getting access token...")
			userInfos = self.downloadFile("https://me.ryzom.com/api/oauth/user?access_token="+token)
			try:
				self.userInfos = json.loads(userInfos)
			except Exception as e:
				self.error(e)
				pass

		if self.userInfos["result"] == "ko" and  token != "skip":
			if self.first_login:
				self.first_login = False
				self.call_LogIn()
				return
		else:
			self.first_login = False

		if self.state == "updating" or self.state == "checking":
			return
		
		rci = self.getModule("ryzom_config_ini")

		rci.config.refresh()
		## Load saved servers and profiles from ryzom.ini
		try:
			servers = rci.getServersFromIni()
		except:
			time.sleep(2)
			servers = rci.getServersFromIni()

		if self.userInfos["result"] == "ok":
			if self.userInfos["subscription"]:
				subtime = humanize.naturaldelta(parser.parse(self.userInfos["subscription"]["end"]) - dt.datetime.now())
			else:
				subtime = ""
			tmpl = self.getTemplateV2("header", {
				"username": self.userInfos["name"],
				"f2p?": self.userInfos["premium"] == "f2p",
				"premium?": self.userInfos["premium"] == "premium",
				"subtime": subtime, 
				"sub?": self.userInfos["subscription"] and self.userInfos["subscription"]["type"] == "sub",
				"nosub?": self.userInfos["subscription"] and self.userInfos["subscription"]["type"] == "nosub",
				"logged?": True,
				"token" : token})
		else:
			tmpl = self.getTemplateV2("header", {
				"notlogged?": True,
			})
		
		self.setZone("header", tmpl)
		self.showZone("header")

		tmpl = self.getTemplateV2("main", {"button_label": self._("ryzom_client_installer.checking")})
		self.setZone("main", tmpl)
		
		servers_tmpl = self.getTemplateV2("servers", {"servers": self.templatize(servers)})
		#self.setE("#-select-servers", servers_tmpl)
	
		KyssModule.setup(self)
		return self.start()
